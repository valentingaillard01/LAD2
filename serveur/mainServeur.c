#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"
#include "serveur.h"


int main() {
	char *message = NULL;
	struct requete_client rq;
	struct reponse_serveur rq2;
	int nbr_para = 6;
	unsigned long taille = 0; // variable utilisé pour la taille des fichiers
	char * contenu; // variable utilisé pour le contenu de fichier 


	char code_retour[] = "404"; //variabled pour test retour
	char type[] = "GET";
	char payload[1024];
	char login[30];
	int error_msg;
	char error_msg_char[8];
	Initialisation();

	while(1) {
		int fini = 0;
	
		AttenteClient();
	
		while(!fini) {
			message = Reception();

			if(message != NULL) {
				//printf("J'ai recu: %s\n", message);
				pdu_reception(message,nbr_para,&rq);
				printTrame_requete_client(&rq);
				strcpy(rq2.login,rq.login); //cast qui fait boucler pour le test retour
				strcpy(rq2.payload,rq.payload);
				strcpy(rq2.type,rq.type);
				strcpy(rq2.code_retour,"306");
				free(message);
				
				error_msg = check_credentials(rq.login,rq.password);
				if (error_msg == 42)
				{
					// creation requete retour avec l'erreur 42
					break;
				}
				printf("Password ok\n");
			
				// Code de l'administration à mettre ici 
				 if (rq.login == "admin")
				{
				// < Code >
				}



				//Verification existences ressources + prermissions de consultation d'un annuaire //
				
				char filename[strlen(rq.target)];;
				strcpy(filename, rq.target);
				strcat(filename,"-permission.txt");
				printf("%s\n", filename);
				taille =  longueur_fichier(filename);
				if (taille == -1) // si l'annuaire n'existe pas 
				{
					// creation requete retour avec l'erreur 55
					break;
				}


				char *contenu = (char *) malloc(taille); // variable de la taille du fichier
				lecture(filename, contenu, taille);
				if(strstr(contenu, rq.login) != NULL)
				{
				printf("Permission OK\n");
				}
				else 
				{
					// creation requete retour avec l'erreur 21
					break;
				}
				//free(contenu);
				error_msg = traitement(rq.type, rq.ressource, rq.target, rq.attribute, rq.payload, rq2.payload);
				printf("%s\n", rq2.payload);
				strcpy(rq2.type, rq.type);
				strcpy(rq2.login, rq.login);
				snprintf(error_msg_char,8,"%d",error_msg);//Conversion du code errueur en string
				strcpy(rq2.code_retour, error_msg_char);

				if(pdu_response(&rq2)) {
					printf("Erreur d'emission\n");
				}
			} else {
				fini = 1;
			}
		}

		TerminaisonClient();
	}

	return 0;
}
