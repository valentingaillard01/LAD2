#ifndef __SERVEUR_H__
#define __SERVEUR_H__

/* Initialisation.
 * Creation du serveur.
 * renvoie 1 si �a c'est bien pass� 0 sinon
 */
int Initialisation();

/* Initialisation.
 * Creation du serveur en pr�cisant le service ou num�ro de port.
 * renvoie 1 si �a c'est bien pass� 0 sinon
 */
int InitialisationAvecService(char *service);


/* Attends qu'un client se connecte.
 * renvoie 1 si �a c'est bien pass� 0 sinon
 */
int AttenteClient();

/* Recoit un message envoye par le client.
 * retourne le message ou NULL en cas d'erreur.
 * Note : il faut liberer la memoire apres traitement.
 */
char *Reception();

/* Envoie un message au client.
 * Attention, le message doit etre termine par \n
 * renvoie 1 si �a c'est bien pass� 0 sinon
 */
int Emission(char *message);

/* Recoit des donnees envoyees par le client.
 * renvoie le nombre d'octets re�us, 0 si la connexion est ferm�e,
 * un nombre n�gatif en cas d'erreur
 */
int ReceptionBinaire(char *donnees, size_t tailleMax);

/* Envoie des donn�es au client en pr�cisant leur taille.
 * renvoie le nombre d'octets envoy�s, 0 si la connexion est ferm�e,
 * un nombre n�gatif en cas d'erreur
 */
int EmissionBinaire(char *donnees, size_t taille);

int pdu_reception(char *requete,int nbr_para,struct requete_client *ptr_stru);

int check_credentials(char *login, char *password);

int pdu_response(struct reponse_serveur *ptr_stru);

void replace(char *contenu,char *substr,int position);

// lecture du fichier filename dans contenu de longueur taille
int lecture(char *filename, char *contenu, int taille);

// Fonction de calcul de taille d'un fichier
unsigned long longueur_fichier(char *file_name);

// recherche position dans une chaine de caractères
int search(char *contenu, char *cible, int offset); // recherche position dans une chaine de caractères

// fonction qui renvoie la position après avoir parcouru nombre_virgule
int move(char *contenu, int initial, int nombre_virgule);

// fonction de suppression de caractere dans une chaine a partir d'une position jusqu'à un certain caractère passé en paramètre
void erase(char *contenu, int position, char caractere, int emplacement); // emplacement : si 0 -> laisse le caractère de fin

//fonction de traitement de la requete et selon le type de la prise en charge de la payload retour.
int traitement(char *type, char *ressource, char *target, char *attr, char *payload, char *payload_retour);

void printTrame_requete_client (struct requete_client *ptr_stru);

void printTrame_reponse_serveur (struct reponse_serveur *ptr_stru);

/* Ferme la connexion avec le client.
 */

void TerminaisonClient();

/* Arrete le serveur.
 */
void Terminaison();

#endif
