#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <sys/types.h>

#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include <errno.h>

#include "struct.h"
#include "serveur.h"


#define TRUE 1
#define FALSE 0
#define LONGUEUR_TAMPON 4096


#ifdef WIN32
#define perror(x) printf("%s : code d'erreur : %d\n", (x), WSAGetLastError())
#define close closesocket
#define socklen_t int
#endif

/* Variables cachees */

/* le socket d'ecoute */
int socketEcoute;
/* longueur de l'adresse */
socklen_t longeurAdr;
/* le socket de service */
int socketService;
/* le tampon de reception */
char tamponClient[LONGUEUR_TAMPON];
int debutTampon;
int finTampon;


/* Initialisation.
 * Creation du serveur.
 */
int Initialisation() {
	return InitialisationAvecService("13214");
}

/* Initialisation.
 * Creation du serveur en pr�cisant le service ou num�ro de port.
 * renvoie 1 si �a c'est bien pass� 0 sinon
 */
int InitialisationAvecService(char *service) {
	int n;
	const int on = 1;
	struct addrinfo	hints, *res, *ressave;

	#ifdef WIN32
	WSADATA	wsaData;
	if (WSAStartup(0x202,&wsaData) == SOCKET_ERROR)
	{
		printf("WSAStartup() n'a pas fonctionne, erreur : %d\n", WSAGetLastError()) ;
		WSACleanup();
		exit(1);
	}
	memset(&hints, 0, sizeof(struct addrinfo));
    #else
	bzero(&hints, sizeof(struct addrinfo));
	#endif

	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ( (n = getaddrinfo(NULL, service, &hints, &res)) != 0)  {
     		printf("Initialisation, erreur de getaddrinfo : %s", gai_strerror(n));
     		return 0;
	}
	ressave = res;

	do {
		socketEcoute = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
		if (socketEcoute < 0)
			continue;		/* error, try next one */

		setsockopt(socketEcoute, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on));
#ifdef BSD
		setsockopt(socketEcoute, SOL_SOCKET, SO_REUSEPORT, &on, sizeof(on));
#endif
		if (bind(socketEcoute, res->ai_addr, res->ai_addrlen) == 0)
			break;			/* success */

		close(socketEcoute);	/* bind error, close and try next one */
	} while ( (res = res->ai_next) != NULL);

	if (res == NULL) {
     		perror("Initialisation, erreur de bind.");
     		return 0;
	}

	/* conserve la longueur de l'addresse */
	longeurAdr = res->ai_addrlen;

	freeaddrinfo(ressave);
	/* attends au max 4 clients */
	listen(socketEcoute, 4);
	printf("Creation du serveur reussie sur %s.\n", service);

	return 1;
}

/* Attends qu'un client se connecte.
 */
int AttenteClient() {
	struct sockaddr *clientAddr;
	char machine[NI_MAXHOST];

	clientAddr = (struct sockaddr*) malloc(longeurAdr);
	socketService = accept(socketEcoute, clientAddr, &longeurAdr);
	if (socketService == -1) {
		perror("AttenteClient, erreur de accept.");
		return 0;
	}
	if(getnameinfo(clientAddr, longeurAdr, machine, NI_MAXHOST, NULL, 0, 0) == 0) {
		printf("Client sur la machine d'adresse %s connecte.\n", machine);
	} else {
		printf("Client anonyme connecte.\n");
	}
	free(clientAddr);
	/*
	 * Reinit buffer
	 */
	debutTampon = 0;
	finTampon = 0;

	return 1;
}

/* Recoit un message envoye par le serveur.
 */
char *Reception() {
	char message[LONGUEUR_TAMPON];
	int index = 0;
	int fini = FALSE;
	int retour = 0;
	while(!fini) {
		/* on cherche dans le tampon courant */
		while((finTampon > debutTampon) &&
			(tamponClient[debutTampon]!='\n')) {
			message[index++] = tamponClient[debutTampon++];
		}
		/* on a trouve ? */
		if ((index > 0) && (tamponClient[debutTampon]=='\n')) {
			message[index++] = '\n';
			message[index] = '\0';
			debutTampon++;
			fini = TRUE;
#ifdef WIN32
			return _strdup(message);
#else
			return strdup(message);
#endif
		} else {
			/* il faut en lire plus */
			debutTampon = 0;
			retour = recv(socketService, tamponClient, LONGUEUR_TAMPON, 0);
			if (retour < 0) {
				perror("Reception, erreur de recv.");
				return NULL;
			} else if(retour == 0) {
				fprintf(stderr, "Reception, le client a ferme la connexion.\n");
				return NULL;
			} else {
				/*
				 * on a recu "retour" octets
				 */
				finTampon = retour;
			}
		}
	}
	return NULL;
}

/* Envoie un message au client.
 * Attention, le message doit etre termine par \n
 */
int Emission(char *message) {
	int taille;
	if(strstr(message, "\n") == NULL) {
		fprintf(stderr, "Emission, Le message n'est pas termine par \\n.\n");
		return 0;
	}
	taille = strlen(message);
	if (send(socketService, message, taille,0) == -1) {
        perror("Emission, probleme lors du send.");
        return 0;
	}
	printf("Emission de %d caracteres.\n", taille+1);
	return 1;
}


/* Recoit des donnees envoyees par le client.
 */
int ReceptionBinaire(char *donnees, size_t tailleMax) {
	size_t dejaRecu = 0;
	int retour = 0;
	/* on commence par recopier tout ce qui reste dans le tampon
	 */
	while((finTampon > debutTampon) && (dejaRecu < tailleMax)) {
		donnees[dejaRecu] = tamponClient[debutTampon];
		dejaRecu++;
		debutTampon++;
	}
	/* si on n'est pas arrive au max
	 * on essaie de recevoir plus de donnees
	 */
	if(dejaRecu < tailleMax) {
		retour = recv(socketService, donnees + dejaRecu, tailleMax - dejaRecu, 0);
		if(retour < 0) {
			perror("ReceptionBinaire, erreur de recv.");
			return -1;
		} else if(retour == 0) {
			fprintf(stderr, "ReceptionBinaire, le client a ferme la connexion.\n");
			return 0;
		} else {
			/*
			 * on a recu "retour" octets en plus
			 */
			return dejaRecu + retour;
		}
	} else {
		return dejaRecu;
	}
}

/* Envoie des donn�es au client en pr�cisant leur taille.
 */
int EmissionBinaire(char *donnees, size_t taille) {
	int retour = 0;
	retour = send(socketService, donnees, taille, 0);
	if(retour == -1) {
		perror("Emission, probleme lors du send.");
		return -1;
	} else {
		return retour;
	}
}

int pdu_response(struct reponse_serveur *ptr_stru){
	size_t taille = snprintf(NULL, 0, "%s %s %s %s\n",ptr_stru -> type,ptr_stru -> login,ptr_stru -> code_retour,ptr_stru -> payload) + 1; //On récupére la taille théorique de la string
	char  *chaine = malloc(taille); //On alloue un mémoire de la taille de la string
	sprintf(chaine,"%s %s %s %s\n",ptr_stru -> type,ptr_stru -> login,ptr_stru -> code_retour,ptr_stru -> payload); // On écrit pour de bon dans la chaine
	
	if(Emission(chaine)!=1) {
		printf("Erreur d'�mission\n");
		return 1;
	}
}

int pdu_reception(char *requete,int nbr_para,struct requete_client *ptr_stru){ 
    char * str = (char *) malloc( (strlen(requete) + 1) * sizeof(char) );
    strcpy(str,requete); // Copie de la requete (strtok est destructeur)
	
	int i;
	
	int init_size = strlen(str);
	char delim[] = " ";

	char *ptr = strtok(str, delim);

	if(ptr != NULL){
		strcpy(ptr_stru -> type,ptr);
		ptr = strtok(NULL, delim);
	}
	if(ptr != NULL){
		strcpy(ptr_stru -> login,ptr);
		ptr = strtok(NULL, delim);
	}
	if(ptr != NULL){
		strcpy(ptr_stru -> password,ptr);
		ptr = strtok(NULL, delim);
	}
	if(ptr != NULL){
		strcpy(ptr_stru -> ressource,ptr);
		ptr = strtok(NULL, delim);
	}
	if(ptr != NULL){
		strcpy(ptr_stru -> target,ptr);
		ptr = strtok(NULL, delim);
	}
	if(ptr != NULL){
		strcpy(ptr_stru -> attribute,ptr);
		ptr = strtok(NULL, delim);
	}
	if(ptr != NULL){
		strcpy(ptr_stru -> payload,ptr);
		return 1; // success
	}

	return 0; //fail
}
// lecture du fichier filename dans contenu de longueur taille
int lecture(char *filename, char *contenu, int taille)
{
    FILE *fp;
	
	if((fp = fopen(filename, "r")) == NULL) { // ouverture fichier
		return(-1);
	}

	fread(contenu, taille, 1, fp); //lecture du fichier dans contenu
	fclose(fp);
	return(0);
}

// Fonction de calcul de taille d'un fichier
unsigned long longueur_fichier(char *file_name)
{
	FILE *file;

    /* INITIALISATION DES PARAMETRES */

	unsigned long fin = 0;

	// OUVERTURE DU FICHIER

	if ((file = fopen(file_name, "r")) == NULL)
	{
		fputs("Impossible d ouvrir le fichier.", stdout);
	}
	else
	{
		fseek(file,0,SEEK_END);
		fin = ftell(file); //fin du fichier
	}

	fclose(file);

	return fin;
}

void printTrame_requete_client (struct requete_client *ptr_stru){
	printf("Type : %s\n",ptr_stru -> type);
	printf("Login : %s\n",ptr_stru ->login);
	printf("Password : %s\n",ptr_stru -> password);
	printf("Ressource : %s\n",ptr_stru -> ressource);
	printf("Target : %s\n",ptr_stru -> target);
	printf("Attribute : %s\n",ptr_stru -> attribute);
	printf("Payload : %s\n",ptr_stru -> payload);
}

void printTrame_reponse_serveur (struct reponse_serveur *ptr_stru){
	printf("Type : %s\n",ptr_stru -> type);
	printf("Login : %s\n",ptr_stru -> login);
	printf("Code retour : %s\n",ptr_stru -> code_retour);
	printf("Payload : %s\n",ptr_stru -> code_retour);
}

int search(char *contenu, char *cible, int offset) // recherche position dans une chaine de caractères
{
	char haystack[strlen(contenu)];
    strncpy(haystack, contenu+offset, strlen(contenu)-offset);
    char *p = strstr(haystack, cible);
    if (p)
       return p - haystack+offset;
    return -1;
}
// fonction qui renvoie la position après avoir parcouru nombre_virgule
int move(char *contenu, int initial, int nombre_virgule)
{
	int count = 0;
	do { 
		if (contenu[initial] == ',')
		{
			count++;
		}
		initial++;
	}
	while (count != nombre_virgule);
	return initial; // nouvelle position
}

void erase(char *contenu, int position, char caractere, int emplacement) // emplacement : si 0 -> laisse le caractère de fin
{																		 // 			  si 1 -> le supprime
	int i = 0;
	int taille = strlen(contenu);
	int init_position = position;
	while (contenu[position] != caractere) // position de du dernier caractère à supprimé
	{
		position++;
	}
	position = position + emplacement;
	int count = position - init_position; // repositionnement
	i = init_position;
	while(contenu[i] != '\0')
	{
		contenu[i] = contenu[i+count];
		i++;
	}
}
int traitement(char *type, char *ressource, char *target, char *attr, char *payload, char *payload_retour)
{
	int position = 0;
	char filename[] = "";
	strncpy(filename, target, strlen(target));
	strcat(filename, "-annuaire.txt");
	int taille = longueur_fichier(filename);
	char *contenu = (char *) malloc(taille); // variable de la taille du fichier
	lecture(filename, contenu, taille);


	if (strcmp(type, "GET") == 0)
	{
		if (strcmp(ressource, "01") == 0) /* ressource de type annuaire (voir RFC) */
		{
			strncpy(payload_retour, contenu, strlen(contenu));
			free(contenu);
		}
		else if (strcmp(ressource, "10") == 0) // ressource de type ligne
		{

			printf("test1\n");
			position = search(contenu, attr, 0); // recherche de attr dans le contenu
			int fin = position;
			int i = 0;
			char temp[1024];
			
			while(contenu[fin-1] != ';')
			{
				temp[i] = contenu[fin];
				fin++;
				i++;
			}
			
			strncpy(payload_retour, temp, strlen(temp));
			return 11;
		}
		
	}
	else if (strcmp(type, "CREATE") == 0)
	{

		strcat(payload, ",NULL,NULL;"); // ajout de l'element de fin de ligne
		strcat(contenu, "\n");
		strcat(contenu, payload);

		FILE *fp = fopen(filename, "w" );
   		if (fp == NULL){
   			printf("echec ouverture du fichier\n");
   		}
   		fwrite(contenu , 1 , strlen(contenu) , fp );
  		fclose(fp);
	}
		
	else if (strcmp(type, "UPDATE") == 0)
	{
		position = search(contenu,ressource,0);
		if (strcmp(attr,"prenom") == 0)
		{
			erase(contenu, position, ',', 0); // suppression du prenom
			replace(contenu, payload, position); // ecrit dans l'emplacement du prénom
		}
		else if (strcmp(attr,"nom") == 0)
		{
			position = move(contenu, position, 1); // deplacement jusqu'au nom
			erase(contenu, position, ',', 0); // suppression du nom
			replace(contenu, payload, position); // ecrit dans l'emplacement du nom
		}
		else if (strcmp(attr,"mail") == 0)
		{
			position = move(contenu, position, 2); // deplacement jusqu'au mail
			erase(contenu, position, ',', 0); // suppression du mail
			replace(contenu, payload, position); // ecrit dans l'emplacement du mail
		}
		else if (strcmp(attr,"tel") == 0)
		{
			position = move(contenu, position, 3); // deplacement jusqu'au telephone
			erase(contenu, position, ',', 0); // suppression du telephone
			replace(contenu, payload, position); // ecrit dans l'emplacement du telephone
			
		}
		else if (strcmp(attr,"adresse_postale") == 0)
		{
			position = move(contenu, position, 4); // deplacement jusqu'a l'adresse postale
			erase(contenu, position, ';', 0); // suppression de l'adresse postale
			replace(contenu, payload, position); // ecrit dans l'emplacement de l'adresse postale
		}
		FILE *fp = fopen(filename, "w" );
   		if (fp == NULL){
   			printf("echec ouverture du fichier\n");
   		}
   		
   		fwrite(contenu , 1 , strlen(contenu) , fp );
  		fclose(fp);
	
		
	}
	else if (strcmp(type, "DELETE") == 0)
	{
		position = search(contenu, ressource, 0);
		if (strcmp(attr,"prenom") == 0)
		{
			erase(contenu, position, ',', 0); // suppression du prenom
			replace(contenu, "NULL", position);
		}
		else if (strcmp(attr,"nom") == 0)
		{
			position = move(contenu, position, 1); // deplacement jusqu'au nom
			erase(contenu, position, ',', 0); // suppression du nom
			replace(contenu, "NULL", position);
		}
		else if (strcmp(attr,"mail") == 0)
		{
			position = move(contenu, position, 2); // deplacement jusqu'au mail
			erase(contenu, position, ',', 0); // suppression du mail
			replace(contenu, "NULL", position);		}
		else if (strcmp(attr,"tel") == 0)
		{
			position = move(contenu, position, 3); // deplacement jusqu'au telephone
			erase(contenu, position, ',', 0); // suppression du telephone
			replace(contenu, "NULL\0", position);
		}
		else if (strcmp(attr,"adresse_postale") == 0)
		{
			position = move(contenu, position, 4); // deplacement jusqu'a l'adresse postale
			erase(contenu, position, ';', 0); // suppression de l'adresse postale
			replace(contenu, "NULL\0", position);
		}
		FILE *fp = fopen(filename, "w" );
   		if (fp == NULL){
   			printf("echec ouverture du fichier\n");
   		}
   		
   		fwrite(contenu , 1 , strlen(contenu) , fp );
  		fclose(fp);
		
	}
}

int check_credentials(char *login, char *password)
{
	unsigned long taille = 0; // initialisation taille

	char filename[] = "admin-permission.txt"; // nom fichier
	taille = longueur_fichier(filename); // renvoie la longueur du fichier

	char * contenu;	
	contenu = (char *) malloc(taille); // variable de la taille du fichier
	lecture(filename, contenu, taille);

	if(strstr(contenu, login) != NULL)
	{
		char *var = strstr(contenu, login);
		int position = var - contenu; // position de la première lettre du login
		int min_pwd, max_pwd, lg_pwd = 0; // variable de position du début et de la fin et de longueur du password

		while(1)
		{
			if (contenu[position] == ',')
			{
				min_pwd = position;
			}
			if (contenu[position] == ';')
			{
				max_pwd = position;
				break;
			}
			++position;
		}
		lg_pwd = max_pwd - min_pwd; // calcul de la longueur pour init la variable test_pswd 

		char test_pswd[lg_pwd];

		for (int i = min_pwd+1; i < max_pwd; ++i)
		{

			test_pswd[i-min_pwd-1] = contenu[i];
			if (i == (max_pwd-1))
			{
				test_pswd[lg_pwd-1] = '\0';
				break;
			}
			//printf("%c\n", test_pswd[i-min_pwd]);
		}

		if (strcmp(test_pswd, password) == 0)
		{
			return 0;
		}
		else return 42 ;
		
		
	}
	else {return 42 ;}
	free(contenu);
}

void replace(char *contenu,char *substr,int position)
{
	
	int m,n,k,j;

	//strlen(str) function to measure length of the string
	m=strlen(contenu);
	n=strlen(substr);
	int taille_finale = m+n;
	char *temp = (char *) malloc(taille_finale);

	//str is copied into temp from 0 to i
	for(j=0;j<position;j++)
	{
	temp[j]=contenu[j];
	}

	//substr is copied into temp from i to n
	for(j=position,k=0;j<n+position,k<n;j++,k++)
	temp[j]=substr[k];

	//remaining str is copied into temp from n+i to m
	for(j=n+position,k=position;j<m,k<m;j++,k++)
	temp[j]=contenu[k];

	contenu = realloc(contenu, taille_finale);

	for (int count = 0; count < taille_finale; ++count)
	{
		contenu[count] = temp[count];
	}
	free(temp);

}
/* Ferme la connexion avec le client.
 */
void TerminaisonClient() {
	close(socketService);
}

/* Arrete le serveur.
 */
void Terminaison() {
	close(socketEcoute);
}
